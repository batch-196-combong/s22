console.log('Hello David');


// Array Methods

// Mutator Methods
// updates / mutates array

// .push() - push() method will allow us to add an item at the end of our array

let koponanNiEugene = ['Eugene'];

koponanNiEugene.push("Vincent");
console.log(koponanNiEugene);

// returns the value
console.log(koponanNiEugene.push('Dennis'));
console.log(koponanNiEugene);

// pop() - remove the last item. Returns the value.
let removedItem = koponanNiEugene.pop();
console.log(koponanNiEugene);
console.log(removedItem);

//.unshift() - allow us to add item at the front of our array.

let fruits = ['Mango', 'Kiwi', 'Apple'];
fruits.unshift('Pineapple');
console.log(fruits);

// .shift() - allows us to remove item at the front
let computerBrands = ['Apple', 'Acer', 'Asus', 'Dell'];
computerBrands.shift();
console.log(computerBrands);

// .splice(startingIndex, deleteCount, insertElement) - remove element from a specified index.

computerBrands.splice(1);
console.log(computerBrands);

fruits.splice(0,1);
console.log(fruits);

koponanNiEugene.splice(1,0,'Dennis','Alfred');
console.log(koponanNiEugene);

fruits.splice(0,2,'Lime','Cherry');
console.log(fruits);

// return the remove items
let item = fruits.splice(0);
console.log(fruits);
console.log(item);

let spiritDetective = koponanNiEugene.splice(0,1);
console.log(spiritDetective);
console.log(koponanNiEugene);

// sort - arrange our elements in an alphanumeric order

let members = ['Ben', 'Alan', 'Alvin', 'Jino', 'Tine'];
members.sort();
console.log(members);

let numbers = [50, 100, 12, 10, 1];
numbers.sort();
console.log(numbers);


members.reverse();
console.log(members);

let carBrands = ['Vios', 'Fortuner', 'Crosswind', 'City', 'Vios','Starex'];
console.log(carBrands);

// indexOf() return the index number of the first match of the element;

let firstIndexOfVios = carBrands.indexOf('Vios');
console.log(firstIndexOfVios);

let indexOfBeetle = carBrands.indexOf('Beetle');
console.log(indexOfBeetle);

// lastIndexOf()
// returns the index number of the last matching element

let lastIndexOfVios = carBrands.lastIndexOf('Vios');
console.log(lastIndexOfVios);

let indexOfMio = carBrands.lastIndexOf('Mio');
console.log(indexOfMio);

// slice()
// copy a slice/portion of an array and return a new array from it. 

let shoeBrand = ['Jordan', 'Nike', 'Adidas', 'Converse', 'Sketchers'];

let myOwnedShoes = shoeBrand.slice(1);
console.log(myOwnedShoes);
console.log(shoeBrand);

let herOwnedShoes = shoeBrand.slice(2,4);
console.log(herOwnedShoes);

let heroes = ['Captain America', 'Superman', 'Spiderman', 'Wonder Woman', 'Hulk', 'Hawkeye', 'Dr. Strange'];

let MyFavoriteHeroes = heroes.slice(2,6);
console.log(MyFavoriteHeroes);

// toString()
// return our array as a string separated by commas
let superHeroes = heroes.toString();
console.log(superHeroes);

// join() - returns our array as a string by specified separator

// without separator
let superHeroes2 = heroes.join();
console.log(superHeroes2); 

let superHeroes3 = heroes.join(', ');
console.log(superHeroes3); 

let superHeroes4 = heroes.join(1);
console.log(superHeroes4); 

// Iteration Methods
// This methods iterates or loops over the items in an array.

//forEach()
// Similar to for loop wherein it is able to iterate over the items in an array.
// It is able to repeat an action FOR EACH item in the array. 

let counter = 0;
heroes.forEach(function(hero){
	counter++
	console.log(counter)
	console.log(hero);
})


let numArr = [5,12,30,46,50]

numArr.forEach(function(number){
	if(number%5 === 0){
		console.log(number + ' is divisible by 5.')
	} else {
		console.log(number + ' is not divisible by 5.')
	}
})

// map()
// it will iterate over all items in an array and run a function for each item. However, with map, whatever is returned,  in the function will be added into a ew array we can save. 

let instructors = members.map(function(member){
	return member + ' is an instructor';
})

console.log(instructors);

numArr2 = [1,2,3,4,5];

let squareMap = numArr2.map(function(number){
	return number * number;
})

console.log(squareMap);

let isAMember = members.includes('Tine');
console.log(isAMember);

let isAMember2 = members.includes('Tee Jae');
console.log(isAMember2);